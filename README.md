# REST сервис простого чата
Это учебный проект, в котором я учусь создавать REST сервис.
По мере развития, я буду добавлять ссылки на испоьзованные сервисы и материалы.

#### Туториалы:
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Testing the Web Layer](https://spring.io/guides/gs/testing-web/)

#### Фреймворки:
* [Spring Boot](https://spring.io/projects/spring-boot)
* Junit 5

#### Общее:
Для быстроты и удобства разработки выбрал H2 в качестве базы данных.
Для работы с базой данных использовал JPA.  
Чтобы уменьшить количество кода подключил [Lombok](https://projectlombok.org/).

#### TODO:
* [ ] Реализовать регистрацию и авторизацию
* [ ] Создать отдельный класс для сообщений об ошибках
* [ ] Подключить [Swagger](https://swagger.io/)