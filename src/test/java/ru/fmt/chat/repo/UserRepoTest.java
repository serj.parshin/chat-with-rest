package ru.fmt.chat.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.fmt.chat.AbstractTest;
import ru.fmt.chat.domain.User;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserRepoTest extends AbstractTest {

    @Autowired
    private UserRepo userRepo;

    @Test
    @Transactional
    void initRepo() {
        String login = "login";
        String name = "Vasya";
        long before = userRepo.count();

        var user = new User().setId(login).setName(name).setPassword("any");
        userRepo.save(user);

        User foundUser = userRepo.findById(login).get();
        assertAll(
                () -> assertEquals(before + 1L, userRepo.count()),
                () -> assertEquals(name, foundUser.getName())
        );
    }

}