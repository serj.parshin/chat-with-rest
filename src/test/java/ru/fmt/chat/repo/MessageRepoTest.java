package ru.fmt.chat.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.fmt.chat.AbstractTest;
import ru.fmt.chat.domain.Message;
import ru.fmt.chat.domain.User;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageRepoTest extends AbstractTest {

    @Autowired
    private MessageRepo messageRepo;

    @Autowired
    private UserRepo userRepo;

    @Test
    @Transactional
    void initRepo() {
        User from = new User().setId("login").setName("vasya");
        User to = new User().setId("acc").setName("petya");
        userRepo.saveAndFlush(from);
        userRepo.saveAndFlush(to);

        String text = "Some message";
        Message message = new Message(text, from, to);
        long before = messageRepo.count();
        messageRepo.save(message);
        assertEquals(before + 1L, messageRepo.count());
    }
}