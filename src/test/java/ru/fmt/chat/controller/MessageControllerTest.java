package ru.fmt.chat.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import ru.fmt.chat.AbstractTest;
import ru.fmt.chat.domain.Message;
import ru.fmt.chat.domain.User;
import ru.fmt.chat.domain.dto.MessageDTO;
import ru.fmt.chat.exception.MessageNotFoundException;
import ru.fmt.chat.repo.MessageRepo;
import ru.fmt.chat.repo.UserRepo;

import java.time.Instant;
import java.util.List;
import java.util.Random;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Transactional
@DisplayName("Тестирование контролера сообщений")
class MessageControllerTest extends AbstractTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MessageController messageController;
    @Autowired
    private MessageRepo messageRepo;
    @Autowired
    private UserRepo userRepo;

    private String firstUserId = "first";
    private String firstUserName = "Vasya";
    private User firstUser = new User(firstUserId, firstUserName, "any");
    private String secondUserId = "second";
    private String secondUserName = "Petya";
    private User secondUser = new User(secondUserId, secondUserName, "any");
    private String textMessageFirst = "First to Second text message";
    private String textMessageSecond = "Second to First text message";
    private Message messageFromFirstToSecond = new Message(textMessageFirst, firstUser, secondUser);
    private Message messageFromSecondToFirst = new Message(textMessageSecond, secondUser, firstUser);
    private static final String URL = "/messages";
    private static final String URL_WITH_ID = "/messages/{id}";

    @Test
    void contextLoads() {
        assertAll(
                () -> assertNotNull(userRepo, "UserRepo пустой"),
                () -> assertNotNull(messageRepo, "MessageRepo пустой"),
                () -> assertNotNull(messageController, "MessageController пустой")
        );
    }

    @BeforeEach
    void initData() {
        userRepo.saveAndFlush(firstUser);
        userRepo.saveAndFlush(secondUser);
    }

    @Test
    @DisplayName("Запрос на получение одного сообщения")
    void one() throws Exception {
        Long firstMessageId = messageRepo.saveAndFlush(messageFromFirstToSecond).getId();
        Instant date = messageRepo.findById(firstMessageId)
                .orElseThrow(() -> new MessageNotFoundException(firstMessageId))
                .getDate();

        mockMvc.perform(get(URL_WITH_ID, firstMessageId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(firstMessageId))
                .andExpect(jsonPath("$.text").value(textMessageFirst))
                .andExpect(jsonPath("$.from").value(firstUser))
                .andExpect(jsonPath("$.to").value(secondUser))
                .andExpect(jsonPath("$.date").value(date.toString()));
    }

    @Test
    @DisplayName("Запрос несуществующего сообщения")
    void getNotExistsMessage() throws Exception {
        String exceptionMessage = "Сообщение с id: 2 не существует";
        mockMvc.perform(get(URL_WITH_ID, 2))
                .andExpect(status().isNotFound())
                .andExpect(content().string(exceptionMessage))
                .andDo(print())
                .andReturn();
    }

    @Test
    @DisplayName("Запрос на получение всех сообщений")
    void all() throws Exception {
        Long firstMessageId = messageRepo.saveAndFlush(messageFromFirstToSecond).getId();
        Long secondMessageId = messageRepo.saveAndFlush(messageFromSecondToFirst).getId();

        Instant firstDate = messageRepo.findById(firstMessageId).get().getDate();
        Instant secondDate = messageRepo.findById(secondMessageId).get().getDate();

        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$[*]").value(hasSize(2)))
                .andExpect(jsonPath("$[*].id").value(hasItems(firstMessageId.intValue(), secondMessageId.intValue())))
                .andExpect(jsonPath("$[*].text").value(hasItems(textMessageFirst, textMessageSecond)))
                .andExpect(jsonPath("$[*].from.id").value(hasItems(firstUser.getId(), secondUser.getId())))
                .andExpect(jsonPath("$[*].from.name").value(hasItems(firstUser.getName(), secondUser.getName())))
                .andExpect(jsonPath("$[*].to.id").value(hasItems(firstUser.getId(), secondUser.getId())))
                .andExpect(jsonPath("$[*].to.name").value(hasItems(firstUser.getName(), secondUser.getName())))
                .andExpect(jsonPath("$[*].date").value(hasItems(firstDate.toString(), secondDate.toString())));
    }

    @Test
    @DisplayName("Запрос на создание нового сообщения")
    void newMessage() throws Exception {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setText(textMessageFirst).setFrom(firstUserId).setTo(secondUserId);
        byte[] bytes = objectMapper.writeValueAsBytes(messageDTO);
        long beforeReq = messageRepo.count();

        MvcResult response = mockMvc.perform(post(URL)
                .content(bytes)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.text").value(textMessageFirst))
                .andExpect(jsonPath("$.from").value(firstUserId))
                .andExpect(jsonPath("$.to").value(secondUserId))
                .andReturn();

        MessageDTO messageDTOFromResponse = objectMapper.readValue(response.getResponse().getContentAsString(), MessageDTO.class);

        List<Message> allMessages = messageRepo.findAll();
        Message messageFromDb = allMessages.get(allMessages.size() - 1);

        assertAll(
                () -> assertEquals(beforeReq + 1, messageRepo.count()),
                () -> assertEquals(textMessageFirst, messageFromDb.getText()),
                () -> assertEquals(firstUserId, messageFromDb.getFrom().getId()),
                () -> assertEquals(secondUserId, messageFromDb.getTo().getId()),
                () -> assertEquals(messageFromDb.getDate(), messageDTOFromResponse.getDate())
        );
    }

    @Test
    @DisplayName("Обновление сообщения")
    void update() throws Exception {
        MessageDTO messageDTO = new MessageDTO();
        String newTextMessage = "New text message";

        Long id = messageRepo.save(new Message(textMessageFirst, firstUser, secondUser)).getId();
        messageDTO.setId(id).setText(newTextMessage).setFrom(firstUserId).setTo(secondUserId);
        byte[] bytes = objectMapper.writeValueAsBytes(messageDTO);

        long beforeReq = messageRepo.count();
        MvcResult response = mockMvc.perform(put(URL)
                .content(bytes)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.text").value(newTextMessage))
                .andExpect(jsonPath("$.from").value(firstUserId))
                .andExpect(jsonPath("$.to").value(secondUserId))
                .andReturn();

        MessageDTO messageDTOFromResponse = objectMapper.readValue(response.getResponse().getContentAsString(), MessageDTO.class);

        List<Message> allMessages = messageRepo.findAll();
        Message messageFromDb = allMessages.get(allMessages.size() - 1);

        assertAll(
                () -> assertEquals(beforeReq, messageRepo.count()),
                () -> assertEquals(newTextMessage, messageFromDb.getText()),
                () -> assertEquals(firstUserId, messageFromDb.getFrom().getId()),
                () -> assertEquals(secondUserId, messageFromDb.getTo().getId()),
                () -> assertEquals(messageFromDb.getDate(), messageDTOFromResponse.getDate())
        );
    }

    @Test
    @DisplayName("Обновление несуществующего сообщения")
    void updateNotExistsMessage() throws Exception {
        Long random = new Random().nextLong();
        String exceptionMessage = "Сообщение с id: "+ random +" не существует";

        MessageDTO messageDTO = new MessageDTO();
        String newTextMessage = "Это сообщение не существует";
        messageRepo.save(new Message(textMessageFirst, firstUser, secondUser));

        messageDTO.setId(random).setText(newTextMessage).setFrom(firstUserId).setTo(secondUserId);
        byte[] bytes = objectMapper.writeValueAsBytes(messageDTO);

        mockMvc.perform(put(URL)
                .content(bytes)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound())
                .andExpect(content().string(exceptionMessage))
                .andDo(print())
                .andReturn();
    }

    @Test
    @DisplayName("Запрос на удаление сообщения")
    void deleteMessage() throws Exception {
        Long id = messageRepo.save(new Message(textMessageFirst, firstUser, secondUser)).getId();

        long beforeReq = messageRepo.count();
        mockMvc.perform(delete(URL_WITH_ID, id))
                .andExpect(status().isNoContent()).andDo(print());

        assertFalse(messageRepo.existsById(id));
        assertEquals(beforeReq-1, messageRepo.count());
    }

    @Test
    @DisplayName("Удаление несуществующего сообщения")
    void deleteNotExistsMessage() throws Exception {
        String exceptionMessage = "Сообщение с id: 2 не существует";
        mockMvc.perform(delete(URL_WITH_ID, 2))
                .andExpect(status().isNotFound())
                .andExpect(content().string(exceptionMessage))
                .andDo(print())
                .andReturn();
    }
}