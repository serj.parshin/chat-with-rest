package ru.fmt.chat.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import ru.fmt.chat.AbstractTest;
import ru.fmt.chat.domain.User;
import ru.fmt.chat.repo.UserRepo;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Тесты рест контролера пользователей")
@Transactional
class UserControllerTest extends AbstractTest {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private UserController userController;

    @Test
    void contextLoads() {
        assertAll(
                () -> assertNotNull(userRepo, "UserRepo пустой"),
                () -> assertNotNull(userController, "UserController пустой")
        );
    }

/*
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Transactional
    @DisplayName("Запрос на получение одного пользователя")
    void one() throws Exception {
        var id = "first";
        var name = "Vasya";
        var user = new User(id, name, "any");
        userRepo.saveAndFlush(user);

        var userFromResponse = this.restTemplate.getForObject("http://localhost:" + port + "/users/" + id, String.class);

        ObjectMapper mapper = new ObjectMapper();
        User gottenUser = mapper.readValue(userFromResponse, User.class);
        System.out.println(gottenUser);
        assertAll(
                () -> assertNotEquals(userFromResponse, "Пользователь с id: " + id + " не существует"),
                () -> assertEquals(userFromResponse, user)
        );
    }
*/

    private String firstUserId = "first";
    private String firstUserName = "Vasya";
    private User firstUser = new User(firstUserId, firstUserName, "any");

    private String secondtUserId = "second";
    private String secondUserName = "Petya";
    private User secondUser = new User(secondtUserId, secondUserName, "any");

    @BeforeEach
    void initData(){
        userRepo.saveAndFlush(firstUser);
        userRepo.saveAndFlush(secondUser);
    }

    @Test
    @DisplayName("Запрос на получение одного пользователя")
    void one() throws Exception {
        mockMvc.perform(get("/users/{id}", firstUserId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(firstUserId))
                .andExpect(jsonPath("$.name").value(firstUserName))
                .andExpect(jsonPath("$.password").value("any"));
    }

    @Test
    @DisplayName("Запрос на получение всех пользователей")
    void all() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MvcResult result = mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$[*]").value(hasSize(2)))
                .andExpect(jsonPath("$[*].id").value(hasItems(firstUserId, secondtUserId)))
                .andReturn();
        User[] users = mapper.readValue(result.getResponse().getContentAsString(), User[].class);
        Set<String> userIdSet = Arrays.stream(users).map(User::getId).collect(Collectors.toSet());
        assertAll(
                () -> assertEquals(2, users.length),
                () -> assertTrue(userIdSet.containsAll(Arrays.asList(firstUserId, secondtUserId)))
        );
    }

}