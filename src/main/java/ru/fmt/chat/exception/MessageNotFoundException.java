package ru.fmt.chat.exception;

public class MessageNotFoundException extends RuntimeException {
    public MessageNotFoundException(Long messageId){
        super("Сообщение с id: " +messageId+ " не существует");
    }
}
