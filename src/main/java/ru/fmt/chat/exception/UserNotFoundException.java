package ru.fmt.chat.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String userId){
        super("Пользователь с id: " + userId + " не существует");
    }
}
