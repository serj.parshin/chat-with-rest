package ru.fmt.chat.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "users", uniqueConstraints = @UniqueConstraint(name = "unique_login", columnNames = "id"))
public class User {

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;

    public User(String id, String name, String password){
        this.id = id;
        this.name = name;
        this.password = password;
    }
}
