package ru.fmt.chat.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "messages", uniqueConstraints = @UniqueConstraint(name = "unigue_id", columnNames = "id"))
public class Message {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "text")
    private String text;
    @ManyToOne
    @JoinColumn(name = "from_user")
    private User from;
    @ManyToOne
    @JoinColumn(name = "to_user")
    private User to;
    @Column(name = "date")
    private Instant date = Instant.now();

    public Message(String text, User from, User to){
        this.text = text;
        this.from = from;
        this.to = to;
    }
}
