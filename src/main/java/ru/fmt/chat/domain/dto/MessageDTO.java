package ru.fmt.chat.domain.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Instant;

@Data
@Accessors(chain = true)
public class MessageDTO {
    private Long id;
    private String text;
    private String from;
    private String to;
    private Instant date;
}
