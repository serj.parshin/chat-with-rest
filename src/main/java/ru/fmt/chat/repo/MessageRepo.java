package ru.fmt.chat.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.fmt.chat.domain.Message;

public interface MessageRepo extends JpaRepository<Message, Long> {
}
