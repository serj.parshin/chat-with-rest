package ru.fmt.chat.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.fmt.chat.domain.Message;
import ru.fmt.chat.domain.User;
import ru.fmt.chat.repo.MessageRepo;
import ru.fmt.chat.repo.UserRepo;

@Profile("!test")
@Configuration
@Slf4j
public class DatabaseConfig {


    private UserRepo userRepo;
    private MessageRepo messageRepo;

    @Autowired
    public DatabaseConfig(UserRepo userRepo, MessageRepo messageRepo){
        this.userRepo = userRepo;
        this.messageRepo = messageRepo;
    }

    @Bean
    CommandLineRunner initUsersAndMessages() {
        return args -> {
            User firstUser = userRepo.save(new User("firstUser", "Вася", "any"));
            log.info("Preloading " + firstUser);
            User secondUser =  userRepo.save(new User("secondUser", "Алёша", "any"));
            log.info("Preloading " + secondUser);

            Message messageFromFirst = messageRepo.save(new Message("Message from firstUser to secondUser", firstUser, secondUser));
            log.info("Preloading " + messageFromFirst);
            Message messageFromSecond = messageRepo.save(new Message("Message from secondUser to firstUser", secondUser, firstUser));
            log.info("Preloading " + messageFromSecond);
        };
    }
}
