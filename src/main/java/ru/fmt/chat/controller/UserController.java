package ru.fmt.chat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.fmt.chat.domain.User;
import ru.fmt.chat.exception.UserNotFoundException;
import ru.fmt.chat.repo.UserRepo;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {
    //TODO Реализовать регистрацию и авторизацию
    private final UserRepo userRepo;

    @GetMapping("/users")
    ResponseEntity<List<User>> all() {
        return ResponseEntity
                .ok()
                .body(userRepo.findAll());
    }

    @GetMapping("/users/{id}")
    ResponseEntity<User> one(@PathVariable String id) {
        User user = userRepo.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
        return ResponseEntity
                .ok()
                .body(user);
    }

}
