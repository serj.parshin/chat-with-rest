package ru.fmt.chat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.fmt.chat.domain.Message;
import ru.fmt.chat.domain.User;
import ru.fmt.chat.domain.dto.MessageDTO;
import ru.fmt.chat.exception.MessageNotFoundException;
import ru.fmt.chat.repo.MessageRepo;
import ru.fmt.chat.repo.UserRepo;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/messages")
public class MessageController {
    private final MessageRepo messageRepo;
    private final UserRepo userRepo;

    @GetMapping
    ResponseEntity<List<Message>> all() {
        return ResponseEntity
                .ok()
                .body(messageRepo.findAll());
    }

    @GetMapping("/{id:[\\d]+}")
    ResponseEntity<Message> one(@PathVariable Long id) {
        Message message = messageRepo.findById(id)
                .orElseThrow(() -> new MessageNotFoundException(id));
        return ResponseEntity
                .ok()
                .body(message);
    }

    @PostMapping
    ResponseEntity<MessageDTO> newMessage(@RequestBody MessageDTO requestBody) {
        User fromUser = userRepo.getOne(requestBody.getFrom());
        User toUser = userRepo.getOne(requestBody.getTo());
        Message message = messageRepo.save(new Message(requestBody.getText(), fromUser, toUser));
        MessageDTO messageDTO = requestBody.setDate(message.getDate()).setId(message.getId());
        return ResponseEntity
                .ok()
                .body(messageDTO);
    }

    @PutMapping
    ResponseEntity<MessageDTO> update(@RequestBody MessageDTO requestBody) {
        Long id = requestBody.getId();
        Message message = messageRepo.findById(id)
                .orElseThrow(() -> new MessageNotFoundException(id))
                .setText(requestBody.getText());
        Message savedMessage = messageRepo.save(message);
        MessageDTO messageDTO = requestBody.setDate(savedMessage.getDate()).setId(savedMessage.getId());
        return ResponseEntity
                .ok()
                .body(messageDTO);
    }

    @DeleteMapping("/{id:[\\d]+}")
    ResponseEntity<Void> delete(@PathVariable Long id) {
        Message messageToDelete = messageRepo.findById(id)
                .orElseThrow(() -> new MessageNotFoundException(id));
        messageRepo.delete(messageToDelete);
        return ResponseEntity.noContent().build();
    }
}
